$(document).ready(function () {
    $('.pricing-button').click(function () {
        $('#myModal').modal('show');
    });
});

$(document).ready(function () {
    const userSlider = document.getElementById("userSlider");
    const userCountDisplay = document.getElementById("userCount");

    function highlightPlan(userCount) {
        $(".pricing-plan").css("background-color", "");
    
        $(".pricing-plan").each(function () {
            const min = parseInt($(this).data("range-min"));
            const max = parseInt($(this).data("range-max"));
            if (userCount >= min && userCount <= max) {
                $(this).css("background-color", "lightyellow");
                return false; 
            }
        });
    }

    const freeDiv = document.getElementById("free");
    const proDiv = document.getElementById("pro");
    const enterprisediv = document.getElementById("enterprise");

    userSlider.addEventListener("input", function () {
        const userCount = parseInt(userSlider.value);
        userCountDisplay.textContent = userCount;
        
        if (userSlider.value <= 10) {
            proDiv.style.boxShadow = "0px 0px";
            enterprisediv.style.boxShadow = "0px 0px";
        } else if (userSlider.value <= 20) {
            freeDiv.style.boxShadow = "0px 0px";
            enterprisediv.style.boxShadow = "0px 0px";
        } else {
            freeDiv.style.boxShadow = "0px 0px";
            proDiv.style.boxShadow = "0px 0px";
        }
        
        console.log(userSlider.value, "slidervalue");
        highlightPlan(userCount);
    });
});

document.getElementById('submitButton').addEventListener('click', function () {
    var form = document.getElementById('myForm');
    form.submit();
});
